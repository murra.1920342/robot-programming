# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/messages/base_sensor_message.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/messages/base_sensor_message.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/messages/odometry_message.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/messages/odometry_message.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/messages/pose_message.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/messages/pose_message.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/blob.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/blob.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/deserializer.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/deserializer.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/id_context.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/id_context.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/id_placeholder.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/id_placeholder.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/identifiable.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/identifiable.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/object_data.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/object_data.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/property.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/property.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/property_container.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/property_container.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/property_container_manager.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/property_container_manager.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/property_eigen.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/property_eigen.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/property_identifiable.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/property_identifiable.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/serializable.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/serializable.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/serialization_context.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/serialization_context.cpp.o"
  "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/src/utils/serializer.cpp" "/home/luca/Scrivania/Robot_Programming/repo/robot-programming/mini_core/build/src/CMakeFiles/core_library.dir/utils/serializer.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/include/eigen3"
  "../src"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
