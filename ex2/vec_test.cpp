#include "vec_f.h"
#include <iostream>

using namespace std;

int main() {
	VecF v(5);
	VecF u(5);
	v.fill(1);
	u.fill(2);
	cout << "Sum (3): " << (v+u).at(2) << endl;
	cout << "Diff (-1): " << (v-u).at(2) << endl;
	cout << "Scalar Product (5): " << (v*5).at(2) << endl;
	cout << "Dot Product (10): " << v*u << endl;

	return 0;
}
