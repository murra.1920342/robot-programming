#include "vec_f.h"

VecF::VecF():
	size(0),
	els(nullptr) {}

VecF::VecF(int size_):
	size(size_),
	els(new float[size_]) {}

VecF::VecF(const VecF& other): 
	VecF(other.size) {
		for (int i=0; i<other.size; i++) 
			els[i] = other.els[i];
	}

float& VecF::at(int index) const {
	return els[index];
}

void VecF::fill(float f) {
	for (int i=0; i<size; i++)
		els[i] = f;
}

VecF VecF::operator + (const VecF& other) const {
	VecF result(*this);
	for (int i=0; i<size; i++)
		result.els[i] += other.els[i];
	return result;
}

VecF VecF::operator - (const VecF& other) const {
	VecF result(*this);
	for (int i=0; i<size; i++)
		result.els[i] -= other.els[i];
	return result;
}

VecF VecF::operator * (const float f) const {
	VecF result(*this);
	for (int i=0; i<size; i++)
		result.els[i] *= f;
	return result;
}

float VecF::operator * (const VecF& other) const {
	float result = 0;
	for (int i=0; i< size; i++)
		result += els[i] * other.els[i];
	return result;
}
