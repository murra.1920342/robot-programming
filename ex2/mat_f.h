#pragma once
#include "vec_f.h"

struct MatF{

	int rows, cols, dim;
	float* el;

	MatF();

	MatF(int rows, int cols);

	MatF(int dim);

	MatF(const MatF& other);

	~MatF();

	void fill(float f);
 
	void randfill();

	float& at(int row, int col) const;		

	MatF operator + (const MatF& other) const;

	MatF operator - (const MatF& other) const;

	MatF operator * (const float f) const;

	VecF operator * (const VecF& v) const;

	MatF operator * (const MatF& other) const;
};
