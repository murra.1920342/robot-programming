#pragma once

struct VecF {
	
	float* els;
	int size;

	VecF();

	VecF(int size);

	VecF(const VecF& other);

	void fill(float f);

	float& at(int index) const;

	VecF operator + (const VecF& other) const;

	VecF operator - (const VecF& other) const;

	VecF operator * (const float f) const;

	float operator * (const VecF& other) const;
};
