#include "mat_f.h"
#include "vec_f.h"
#include <iostream>

using namespace std;

MatF::MatF():
	rows(0),
	cols(0),
	dim(0),
	el(nullptr) {}
	

MatF::MatF(int rows_, int cols_):
	rows(rows_),
	cols(cols_),
	dim(rows * cols_),
	el(new float[dim]) {}

MatF::MatF(const MatF& other):
	MatF(other.rows, other.cols) {
	for (int i=0; i < dim; i++)
		el[i] = other.el[i];
}	

MatF::~MatF() {
	delete [] el;
}

void MatF::fill(float f) {
	for (int i=0; i < dim; i++)
		el[i] = f;
}

void MatF::randfill() {
	for (int i=0; i < dim; i++)
		el[i] = drand48();
}

float& MatF::at(int row, int col) const {
	return el[row * cols + col];
}

MatF MatF::operator + (const MatF& other) const {
	MatF result(*this);
	for (int i=0; i<dim; i++)
		result.el[i] += other.el[i];
	return result;
}

MatF MatF::operator - (const MatF& other) const {
	MatF result(*this);
	for (int i=0; i<dim; i++)
		result.el[i] -= other.el[i];
	return result;
}	

MatF MatF::operator * (const float f) const {
	MatF result(*this);
	for (int i=0; i< dim; i++)
		result.el[i] *= f;
	return result; 
}

float sum(float* array, int size);

VecF MatF::operator * (const VecF& v) const {
	VecF result(this->rows);
	VecF summer(this->cols);
	for (int i=0; i<rows; i++) {
		for (int j=0; j<cols; j++) 
			summer.at(j) = (*this).at(i, j) * v.at(i);
		result.at(i) = sum(summer.els, this->cols);	
	}

	return result;
}

float sum(float* array, int size) {
	int result = 0;
	for (int i=0; i<size; i++) 
		result += array[i];
	return result;
}
