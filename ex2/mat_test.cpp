#include <iostream>
#include "mat_f.h"
#include "vec_f.h"

using namespace std;

int main() {
	MatF m(2,2);
	m.fill(1);
	MatF n(2,2);
	n.fill(2);
	VecF v(2);
	v.fill(4);

	cout << "Hello World!" << endl;
	cout << "Sum (3): " <<  (m+n).at(1,1) << endl;
	cout << "Scalar Product (5): " << (m*5).at(1,1) << endl;
	cout << "Dot Product (16): " << (n*v).at(1) << endl;
	return 0;
}
